from pyspark import SparkContext
import sys
import os
import re
import itertools as it
import random as rd
import numpy as np
from operator import add
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.classification import SVMWithSGD, SVMModel
from pyspark.mllib.classification import LogisticRegressionWithLBFGS, LogisticRegressionModel


def window_div_fixed(x):
    if x % w_size == 0:
        return x / w_size
    else:
        return x / w_size + 1

def window_div_sliding(x):
    if x < w_size:
        return range(x, x + 1)
    else:
        return range(x - w_size + 1, x)

def window_calc(x):
    temp = map(lambda line: w2v_dic[line], x)
    return np.mean(temp, axis = 0)

def label(x, y):
    if x in target_log_window_code:
        return LabeledPoint(1.0, y)
    else:
        return LabeledPoint(0.0, y)


if __name__ == '__main__':
    sc = SparkContext('local[40]', 'window')

    #inputLog = 'messages-2015021[12].cleansed'
    inputLog = 'controller/*/c0-0c0s1/messages-*'
    inputDic = 'word2vec_dic'

    window_type = 0     # fixed 0, sliding 1
    w_size = 5          # window size
    mlAlgo = 0          #SVM 0, Logistic Regression 1, neural network 2

    #preprocessing
    log = sc.textFile(inputLog).map(lambda strs: re.sub('0x[0-9a-f]+', 'mem_addr', strs)).map(lambda strs: re.sub('[0-9]+', '*', strs))

    #code_log = log.zipWithIndex().coalesce(1).saveAsTextFile('zipped_File_X')


    #code all the logs, let the code start from 1
    zip_log = log.zipWithIndex().mapValues(lambda code: code + 1)
    num_log = zip_log.count()
    #target_log = '<13>1 2015-02-11T17:09:07-06:00 c0-0 cvcd 7278 - -  cpu_load_watermark_error: changed from 0.00 to 30.00 (10.00-20.00-30.00)  (script)'
    target_error = 'cpu_load_watermark_error: changed from *.* to *.*'


    if window_type == 0:
        #read the dictonary of word2vec feature vectors
        w2v_dic = eval(open(inputDic, 'r').read())
        #create start&end special window para

        windows = zip_log.map(lambda (x, y): (y, x))\
            .map(lambda (x, y): (window_div_fixed(x), y))\
            .groupByKey()\
            .mapValues(lambda x: window_calc(x))\
            .sortByKey().cache()
        #.coalesce(1).saveAsTextFile('windows_Fixed')

        #fetching target window array before target_log
        target_log_window_code = zip_log.filter(lambda (x, y): target_error in x).values().map(lambda x: x - w_size).collect()

        if mlAlgo == 0:
            #label the data
            labeled_window = windows.map(lambda (x, y): label(x, y)).cache()
            #SVM model
            SVM_model = SVMWithSGD.train(labeled_window, iterations = 100)
            SVM_labelsAndPreds = labeled_window.map(lambda p: (p.label, SVM_model.predict(p.features)))
            SVM_trainErr = SVM_labelsAndPreds.filter(lambda lp: lp[0] != lp[1]).count() / float(labeled_window.count())
            print("Training Error = " + str(SVM_trainErr))
            with open('SVM_err_fixed', 'w') as f:
                f.write(repr(SVM_trainErr))

        elif mlAlgo == 1:
            # label the data
            labeled_window = windows.map(lambda (x, y): label(x, y)).cache()
            #Logistic Regression
            LG_model = LogisticRegressionWithLBFGS.train(labeled_window)
            LG_labelsAndPreds = labeled_window.map(lambda p: (p.label, LG_model.predict(p.features)))
            LG_trainErr = LG_labelsAndPreds.filter(lambda lp: lp[0] != lp[1]).count() / float(labeled_window.count())
            with open('LG_err_fixed', 'w') as f:
                f.write(repr(LG_trainErr))

        elif mlAlgo == 2:
            windows.saveAsTextFile('windows_for_neural')
            data = spark.read.format("libsvm")\
                .load("windows_for_neural")
            # Split the data into train and test
            splits = data.randomSplit([0.6, 0.4], 1234) #divide and random seed
            train = splits[0]
            test = splits[1]
            layers = [4, 5, 4, 3]
            trainer = MultilayerPerceptronClassifier(maxIter=100, layers=layers, blockSize=128, seed=1234)
            model = trainer.fit(train)
            result = model.transform(test)
            predictionAndLabels = result.select("prediction", "label")
            evaluator = MulticlassClassificationEvaluator(metricName="accuracy")
            print("Test set accuracy = " + str(evaluator.evaluate(predictionAndLabels)))





    elif window_type == 1:
        #read the dictonary of word2vec feature vectors
        w2v_dic = eval(open(inputDic, 'r').read())

        windows = zip_log.map(lambda (x, y): (x, window_div_sliding(y)))\
            .flatMapValues(lambda x: x)\
            .map(lambda (x, y): (y, x))\
            .groupByKey()\
            .mapValues(lambda x: window_calc(x))\
            .sortByKey().cache()
        #.coalesce(1).saveAsTextFile('windows_Sliding')

        #fetching target window array before target_log
        target_log_window_code = zip_log.filter(lambda (x, y): target_error in x).values().map(lambda x: x - w_size).collect()

        if mlAlgo == 0:
            #label the data
            labeled_window = windows.map(lambda (x, y): label(x, y)).cache()
            #SVM model
            SVM_model = SVMWithSGD.train(labeled_window, iterations = 100)
            SVM_labelsAndPreds = labeled_window.map(lambda p: (p.label, SVM_model.predict(p.features)))
            SVM_trainErr = SVM_labelsAndPreds.filter(lambda lp: lp[0] != lp[1]).count() / float(labeled_window.count())
            print("Training Error = " + str(SVM_trainErr))
            with open('SVM_err_sliding', 'w') as f:
                f.write(repr(SVM_trainErr))

        elif mlAlgo == 1:
            # label the data
            labeled_window = windows.map(lambda (x, y): label(x, y)).cache()
            #Logistic Regression
            LG_model = LogisticRegressionWithLBFGS.train(labeled_window)
            LG_labelsAndPreds = labeled_window.map(lambda p: (p.label, LG_model.predict(p.features)))
            LG_trainErr = LG_labelsAndPreds.filter(lambda lp: lp[0] != lp[1]).count() / float(labeled_window.count())
            with open('LG_err_Sliding', 'w') as f:
                f.write(repr(LG_trainErr))










